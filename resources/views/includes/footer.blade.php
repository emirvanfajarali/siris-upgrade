<div class="footer pt-5 pb-4">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <p class="text-bold text-center">Copyright {{date("Y")}} Siris Developer<p>
            </div>
            <div class="col-md-2 col-xs-12">
                <a href="#" class="text-black"><p class="text-center"> Privacy and Policy </p></a>
            </div>
            <div class="col-md-2 col-xs-12">
                <a href="#" class="text-black"><p class="text-center"> Terms of Use </p></a>
            </div>
            <div class="col-md-2 col-xs-12">
                <div class="text-center">
                    <img class="icons" src="{{asset('assets/icon/facebook.svg')}}"></a>
                    <img class="icons" src="{{asset('assets/icon/instagram.png')}}"></a>
                    <img class="icons" src="{{asset('assets/icon/youtube.svg')}}"></a>
                </div>
            </div>
        </div>
    </div>
</div>
