<nav class="my-nav navbar navbar-dark navbar-expand-lg">
    <div class="container-fluid">
        <div class="brand-centered">
            <a class="navbar-brand" href="{{url('/')}}">
                <div class="logo">
                    <img src="{{asset('assets/image/logo.svg')}}" class=" img-logo logo-neumorphism">
                </div>
            </a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('about')}}">ABOUT</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('work')}}">WORKS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('gallery')}}">GALLERY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('contact')}}">CONTACT</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
