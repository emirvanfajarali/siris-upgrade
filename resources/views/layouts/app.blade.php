<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="siris">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <!-- css -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <link href="//fonts.googleapis.com/css?family=Anton" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/bootstrap-4.0.0-dist/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/fontawesome-free-5.12.1-web/css/all.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/slick/slick-master/slick/slick.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/slick/slick-master/slick/slick-theme.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/css/custom.css')}}" />
    @yield('style')
</head>

<body class="bg-white">
    <header>
        @include('includes.header')
    </header>
        @yield('content')
    <footer>
        @include('includes.footer')
    </footer>


    <script type="text/javascript" src="{{asset('assets/vendor/jquery-3.1.0.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendor/bootstrap-4.0.0-dist/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendor/slick/slick-master/slick/slick.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    @yield('script')

</body>
</html>
