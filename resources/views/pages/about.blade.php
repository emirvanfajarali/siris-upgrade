@extends('layouts.app')
@section('content')
    <div class="container-fluid aboutus-bg-fullscreen">
        <div>
            <h1 class="centered-text about-title color-white">
              ABOUT US
            </h1>
            <h2 class="centered-text about-title-1 color-white">
              We are a Software Developer Company based on Jakarta, Indonesia
            </h2>
            <h5 class="centered-text about-title-2 color-white">
              We help business and organization create their custom software for mobile, desktop, and web applications.
              Take the chance to evolve your business for the future.
            </h5>
        </div>

        <!-- card horizontal -->
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                  <div class="col-md-10">
                      <div class="card inner grow ">
                          <div class="row no-gutters">
                              <div class="col-md-6">
                                <div class="inner">
                                  <img src="{{asset('assets/image/cardimage1.png')}}" class="card-img fade" alt="...">
                                </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="card-body card-middle-text">
                                    <h1 class="card-title color-pink about-vision grow">Our Vision</h1>
                                    <p class="card-text">Become the master of what we do best. </p>
                                  </div>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
          <br><br><br><br>
          <div class="row">
            <!-- <div class="col-md-8"> -->
                <div class="card inner grow">
                    <div class="row no-gutters">
                        <div class="col-md-6">
                            <div class="card-body card-middle-text">
                              <h1 class="card-title text-center color-purple about-goal grow">Our Goal</h1>
                              <p class="card-text text-center">To be more than just creator.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                          <div class="inner">
                            <img src="{{asset('assets/image/cardimage2.png')}}" class="card-img fade" alt="...">
                          </div>
                        </div>
                    </div>
                  </div>
              <!-- </div> -->
          </div>
          <br><br><br><br>
        </div>

        <!-- card deck workflow -->
        <div class="container-fluid">
            <h1 class="centered-text about-title color-orange">
              Our Workflow
            </h1>

            <div class="card-deck">
                <div class="card animate-bold">
                  <div class="inner">
                    <img class="card-img-top border-radius" src="{{asset('assets/image/carddeck1.png')}}" alt="Card image cap">
                  </div>
                  <div class="card-body border-radius">
                    <p class="card-title">Insight</p>
                    <p class="card-text">The pre-production process that we conduct before production began.</p>
                  </div>
                </div>
                <div class="card animate-bold">
                  <div class="inner">
                    <img class="card-img-top border-radius" src="{{asset('assets/image/carddeck2.png')}}" alt="Card image cap">
                  </div>
                  <div class="card-body border-radius">
                    <p class="card-title">Painting</p>
                    <p class="card-text">Here we define how it looks, how it feels, and how it supposed to be</p>
                  </div>
                </div>
                <div class="card animate-bold">
                  <div class="inner">
                    <img class="card-img-top border-radius" src="{{asset('assets/image/carddeck3.png')}}" alt="Card image cap">
                  </div>
                  <div class="card-body border-radius">
                    <p class="card-title">Forging</p>
                    <p class="card-text">And now the hardwork began. This is where the product will take shape before ready to use</p>
                  </div>
                </div>
              </div>
        </div>
        <br><br><br><br>

      </div>


@stop
