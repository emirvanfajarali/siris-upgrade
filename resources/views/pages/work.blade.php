@extends('layouts.app')
@section('content')
<div class="container bg-white">
    <div class="row">
        <div class="col-md-12">
            <h1 class="section-title text-center mt-9 mb-5"> WORKS </h1>
            <p class="section-sub-30 text-center mb-5"> Here is our notable work </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="neumorphism">
                <img class="img-works" src="{{asset('assets/image/zeelandia.png')}}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="neumorphism">
                <img class="img-works" src="{{asset('assets/image/kpujakpus.png')}}" style="height:160px;">
            </div>
        </div>
        <div class="col-md-4">
            <div class="neumorphism">
                <img class="img-works" src="{{asset('assets/image/the-kiddos.png')}}">
            </div>
        </div>
    </div>
</div>
<br><br><br><br>
@endsection
