@extends('layouts.app')
@section('style')
    <style>
        .slick-list {
            padding: 0 !important;
        }

        .tr-text-slider {
            position: absolute;
            left: 55%;
            top: 0;
        }
    </style>
@endsection
@section('content')
<div class="row no-gutters">
    <div class="col-md-12">
        <div class="slider">
            <div class="item">
                <div class="row no-gutters">
                    <div class="col-md-6">
                        <span class="display-0 text-slider-title"> We can do it for you </span>
                        <span class="display-0 text-slider-sub"> Not sure you can build a custom software for your business by yourself? We can do it for you. </span>
                        <img src="{{asset('assets/image/slide_1.png')}}" class="mr-auto ml-auto img-slider"/>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row no-gutters">
                    <div class="col-md-6 ml-auto">
                        <span class="display-0 text-slider-title"> Why build a custom software ? </span>
                        <span class="display-0 text-slider-sub"> you can have an app that suit to your spesification and need. </span>
                        <img src="{{asset('assets/image/slide_2.jpg')}}" class="mr-auto ml-auto img-slider"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="tr-text-slider content-slider">
            <div class="content-slider-container">
                <div>
                    <h2 id="text-slider-title"> </h2>
                    <h5 id="text-slider-sub">  </h5>
                </div>
            </div>
        </div>
        <div class="arrow-container">
            <div class="row no-gutters">
                <a href="#" class="col align-self-end text-center prev-slick arrow-left">
                    <i class="fas fa-arrow-left i-arrow color-white"></i>
                </a>
                <a href="#" class="col align-self-end text-center next-slick arrow-right">
                    <i class="fas fa-arrow-right i-arrow color-white"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- about us -->
<div class="container-fluid bg-black" style="margin-top:-10px;">
    <div class="row">
        <div class="col-sm-12">
            <p class="about-title text-center margin-title"> ABOUT US </p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <p class="about-sub-title text-center">
            We are a Software Developer Company based on Jakarta, Indonesia
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <p class="about-description text-center">
            We help business and organization create their custom software for mobile, desktop, and web applications. Take the chance to evolve your business for the future
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="d-flex mb-8">
                <button onclick="window.location.href ='{{url('about')}}';" class="button button-green">
                    <span>More</span>
                    <i class="fas fa-arrow-right i-arrow"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- works -->
<div class="container-fluid bg-white">
    <div class="row">
        <div class="col-md-12">
            <p class="works-title text-center margin-title"> WORKS </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="container-neumorphism">
                <div class="row">
                    <div class="col-md-5 col-xs-12">
                        <div class="neumorphism">
                            <img class="img-works" src="{{asset('assets/image/zeelandia.png')}}">
                        </div>
                    </div>
                    <div class="col-md-5 col-xs-12">
                        <div class="neumorphism">
                            <img class="img-works" src="{{asset('assets/image/kpujakpus.png')}}" style="height:160px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5 pt-5">
            <div class="d-none d-md-block d-lg-block">
                <p class="works-p pt-5">This is our work.</p>
                <p class="works-p">The project that started it all.</p>
                <button onclick="window.location.href ='{{url('work')}}';" class="button button-white">
                    <span>More</span>
                    <i class="fas fa-arrow-right i-arrow color-black"></i>
                </button>
            </div>
        </div>

    </div>
</div>
<!-- gallery -->
<div class="container-fluid bg-black">
    <div class="row">
        <div class="col-sm-6" style="font-family:Anton;">
            <div class="movetxt">
                <div class="row movetxt-left">
                    <p class="text-gallery mt--7"> GAL </p>
                </div>
                <div class="row movetxt-right">
                    <p class="text-gallery mt--9" style="color:#A4A5A7;"> LERY </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6" style="font-family:Montserrat;">
            <div class="row">
                <div class="col-sm-6 p-0">
                    <div class="image-gallery">
                        <img src="{{asset('assets/image/kpu.jpeg')}}" class="img-fluid image-gallery-zoom">
                    </div>
                </div>
                <div class="col-sm-6 p-0">
                    <div class="image-gallery">
                        <img src="{{asset('assets/image/zeemainema.jpeg')}}" class="img-fluid image-gallery-zoom">
                    </div>
                </div>
            </div>
            <div class="row bg-purple">
                <div class="col-md-12">
                    <p class="title-gallery">Learn more about us through pictures</p>
                    <div class="ml-5 mt-3 mb-5">
                        <button onclick="window.location.href ='{{url('gallery')}}';" class="button button-white">
                            <span>Go Ahead</span>
                            <i class="fas fa-arrow-right i-arrow color-black"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- get in touch -->
<div class="container-fluid bg-black">
    <div class="row">
        <div class="col-sm-6">
            <div class="mid-text get-in-touch">
                Get In Touch
            </div>
            <div class="d-none d-md-none d-lg-none d-xl-block">
                <div class="d-flex mt-2 mb-7">
                    <button onclick="window.location.href ='{{url('contact')}}';" class="button button-green">
                        <span>More</span>
                        <i class="fas fa-arrow-right i-arrow"></i>
                    </button>
                </div>
            </div>
            <div class="d-block d-md-block d-lg-block d-xl-none">
                <div class="d-flex mt-9 mb-6">
                    <button class="button button-green">
                        <span>More</span>
                        <i class="fas fa-arrow-right i-arrow"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-6 pt-5">
            <div class="d-block d-md-none">
                    <p class="touch-description text-center">All roads lead to Rome as famous phrases says, and here is the road to get in touch with us.</p>
            </div>
            <div class="d-none d-md-block">
                <div class="pt-5">
                    <p class="touch-description text-center">All roads lead to Rome as famous phrases says, and here is the road to get in touch with us.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function(){
            var items = $(".item").length;
            $("#text-slider-title").text($(".text-slider-title").eq(0).text());
            $("#text-slider-sub").text($(".text-slider-sub").eq(0).text());
            $(".item").eq(0).css({'background-color':'#1D2024'});
            $(".slider").slick({
                dots: false,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                prevArrow: $('.prev-slick'),
                nextArrow: $('.next-slick'),
            });

            $(".slider").on("beforeChange", function(event, slick, currentSlide, nextSlide){
                var counts = nextSlide%items;
                var background = ['#1D2024','#051937','#20CA6D','#85C6FF']
                let contentSlider = $(".content-slider");

                if(!contentSlider.hasClass('tr-text-slider')){
                    contentSlider.addClass('tr-text-slider');
                }
                $("#text-slider-title").text($(".text-slider-title").eq(nextSlide).text());
                $("#text-slider-sub").text($(".text-slider-sub").eq(nextSlide).text());
                $(".item").eq(nextSlide).css({'background-color':background[nextSlide]});
                if(counts == 0){
                    contentSlider.animate({
                        left: '55%',
                        opacity: 0.1,
                    },{
                        queue: false,
                        duration: 300,
                        complete: function(){
                            contentSlider.animate({
                                opacity:1
                            }, 300)
                        }
                    });
                }else if(counts > 0){
                    contentSlider.animate({
                        left: '5%',
                        opacity: 0.1,
                    },{
                        queue: false,
                        duration: 300,
                        complete: function(){
                            contentSlider.animate({
                                opacity:1
                            }, 300)
                        }
                    });
                }

                // if(contentSlider.hasClass)
            });
        });
    </script>
@endsection
