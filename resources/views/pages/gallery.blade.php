@extends('layouts.app')
@section('content')
<div class="gallery-section">
  <div class="inner-width">
    <h1>Our Gallery</h1>
    <div class="border"></div>
    <div class="gallery">

      <a href="{{asset('assets/image/1.png')}}" class="image">
        <img src="{{asset('assets/image/1.png')}}" alt="">
      </a>

      <a href="{{asset('assets/image/2.png')}}" class="image">
        <img src="{{asset('assets/image/2.png')}}" alt="">
      </a>

      <a href="{{asset('assets/image/3.png')}}" class="image">
        <img src="{{asset('assets/image/3.png')}}" alt="">
      </a>

      <a href="{{asset('assets/image/4.png')}}" class="image">
        <img src="{{asset('assets/image/4.png')}}" alt="">
      </a>

      <a href="{{asset('assets/image/5.png')}}" class="image">
        <img src="{{asset('assets/image/5.png')}}" alt="">
      </a>

      <a href="{{asset('assets/image/6.png')}}" class="image">
        <img src="{{asset('assets/image/6.png')}}" alt="">
      </a>

      <a href="{{asset('assets/image/7.png')}}" class="image">
        <img src="{{asset('assets/image/7.png')}}" alt="">
      </a>

      <a href="{{asset('assets/image/8.png')}}" class="image">
        <img src="{{asset('assets/image/8.png')}}" alt="">
      </a>

    </div>
  </div>
</div>

@stop

@section('script')
<script>
$(".gallery").magnificPopup({
  delegate: 'a',
  type: 'image',
  gallery:{
    enabled: true
  }
});
</script>
@endsection
